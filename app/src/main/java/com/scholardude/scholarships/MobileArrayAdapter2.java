package com.scholardude.scholarships;

/**
 * Created by krish on 10/27/17.
 */
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class MobileArrayAdapter2 extends ArrayAdapter<String> {
    private final Context context;
//    private final String[] values;
private final List<String> values;


    public MobileArrayAdapter2(Context context, List<String> values) {
        super(context, R.layout.list_mobile, values);
        this.context = context;
        this.values = values;


    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rowView = inflater.inflate(R.layout.list_mobile, parent, false);
        TextView textView = (TextView) rowView.findViewById(R.id.label);
        ImageView imageView = (ImageView) rowView.findViewById(R.id.logo);
//        textView.setText(values[position]);
        textView.setText(values.get(position));

        // Change icon based on name

        String s = values.get(position);

        Log.i("position", s);

        System.out.println(s);

        if (s.equals("WindowsMobile")) {
            imageView.setImageResource(R.drawable.ic_menu_send);
        } else if (s.equals("iOS")) {
            imageView.setImageResource(R.drawable.ic_menu_share);
        } else if (s.equals("Blackberry")) {
            imageView.setImageResource(R.drawable.ic_menu_gallery);
        } else {
            imageView.setImageResource(R.mipmap.ic_application);
        }

        return rowView;
    }
}
