package com.scholardude.scholarships;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class AppliedActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    //
//    static final String[] MOBILE_OS =
//            new String[] { "Android", "iOS", "WindowsMobile", "Blackberry"};
    List<String> MOBILE_OS, names;

    ListView lstView;

    FirebaseDatabase database;

    DatabaseReference myRef;

    ProgressDialog progressDialog;

    String url;
    FirebaseAuth auth;

    private AdView mAdView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_applied);


        MobileAds.initialize(this, "ca-app-pub-4268062457728414/7004806642");
        mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);


        auth = FirebaseAuth.getInstance();

        database = FirebaseDatabase.getInstance();
        myRef = database.getReference("applied").child(auth.getCurrentUser().getUid());
        MOBILE_OS =
                new ArrayList<String>();
        names =
                new ArrayList<String>();

        progressDialog = new ProgressDialog(this, R.style.Theme_AppCompat_DayNight_Dialog);

        myRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                MOBILE_OS.add((String) dataSnapshot.child("url").getValue());

                names.add((String) dataSnapshot.child("name").getValue());

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        myRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                lstView.setAdapter(new MobileArrayAdapter(AppliedActivity.this, MOBILE_OS,names));

                progressDialog.hide();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        lstView = (ListView) findViewById(R.id.lstView);

        lstView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Object item = lstView.getItemAtPosition(position);

                url = item.toString();
                if(!url.startsWith("http")){
                    url = "http://"+url;
                }
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
            }
        });


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                shareapp();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }



    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


private void shareapp() {

    Intent share = new Intent(Intent.ACTION_SEND);
    share.setType("text/plain");
    share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
    share.putExtra(Intent.EXTRA_SUBJECT, "ScholarDude");
    String sharetxt  = "Let me recommend you this application "+ "http://scholardude.com/";
    share.putExtra(Intent.EXTRA_TEXT,  sharetxt);
    startActivity(Intent.createChooser(share, "Share link!"));

}





    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }

        if ((String) item.getTitle() == "reset"){

            resetStateScholarships();

        }
        else {
            filterStateScholarships((String) item.getTitle());
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action

            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);

        } else if (id == R.id.nav_favourite) {
            Intent intent = new Intent(this, FavouriteActivity.class);

            startActivity(intent);

        } else if (id == R.id.nav_share) {

            shareapp();

        } else if (id == R.id.nav_logout){
            auth.signOut();
            Intent intent = new Intent(AppliedActivity.this, LoginActivity.class);
            startActivity(intent);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void resetStateScholarships(){

        MOBILE_OS.clear();

        myRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                MOBILE_OS.add((String) dataSnapshot.child("url").getValue());

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        myRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                lstView.setAdapter(new MobileArrayAdapter2(AppliedActivity.this, MOBILE_OS));

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }

    private  void filterStateScholarships(String state){

        MOBILE_OS.clear();

        myRef.orderByChild("state").equalTo(state).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                Iterable<DataSnapshot> iterdsnp = dataSnapshot.getChildren();
                for (DataSnapshot dtat0: iterdsnp
                     ) {

                    MOBILE_OS.add((String) dtat0.child("url").getValue());

                }

                lstView.setAdapter(new MobileArrayAdapter2(AppliedActivity.this, MOBILE_OS));

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }
}
