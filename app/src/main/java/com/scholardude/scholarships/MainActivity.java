package com.scholardude.scholarships;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    //
//    static final String[] MOBILE_OS =
//            new String[] { "Android", "iOS", "WindowsMobile", "Blackberry"};
    List<String> MOBILE_OS, names;

    ListView lstView;

    FirebaseDatabase database;
    FirebaseAuth auth;

    DatabaseReference myRef;

    ProgressDialog progressDialog;

    private Handler mHandler = new Handler();

    String url, name;


    String json = null;
    JSONObject jsonobj;


    private AdView mAdView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        MobileAds.initialize(this, "ca-app-pub-4268062457728414/7004806642");
        mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);


        auth = FirebaseAuth.getInstance();

        database = FirebaseDatabase.getInstance();
        myRef = database.getReference();
        MOBILE_OS =
                new ArrayList<String>();
        names =
                new ArrayList<String>();


        progressDialog = new ProgressDialog(this, R.style.Theme_AppCompat_DayNight_Dialog);


        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setMessage("Loading scholarships");
        progressDialog.show();


        myRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {


                name = (String) dataSnapshot.child("name").getValue();


                names.add(name);


                url = (String) dataSnapshot.child("url").getValue();

                if (url != null) {
                    url = url.replaceAll("\\s+", "");
                    if (url.trim() != "") {

                        MOBILE_OS.add(url);

                    }
                }

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        myRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                lstView.setAdapter(new MobileArrayAdapter(MainActivity.this, MOBILE_OS, names));
                progressDialog.hide();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        lstView = (ListView) findViewById(R.id.lstView);

        //Register listview for context menu
        registerForContextMenu(lstView);


        lstView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

//                lstView.showContextMenu();

                Object item = lstView.getItemAtPosition(position);

                url = item.toString();
                if (!url.startsWith("http")) {
                    url = "http://" + url;
                }
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
            }
        });


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                shareApp();


            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);

        navigationView.setNavigationItemSelectedListener(this);


        // this listener will be called when there is change in firebase user session
        FirebaseAuth.AuthStateListener authListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user == null) {
                    // user auth state is changed - user is null
                    // launch login activity
                    startActivity(new Intent(MainActivity.this, LoginActivity.class));
                    finish();
                }
            }
        };


        try {

            InputStream is = getAssets().open("states.json");

            int size = is.available();

            byte[] buffer = new byte[size];

            is.read(buffer);

            is.close();

            json = new String(buffer, "UTF-8");
            try {

                jsonobj = new JSONObject(json);

            } catch (JSONException ex) {
                Toast.makeText(this, "Unable to read states", Toast.LENGTH_SHORT).show();
            }

        } catch (IOException ex) {
            ex.printStackTrace();
        }


    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.context_menu, menu);

    }


    @Override
    public boolean onContextItemSelected(MenuItem item) {

//        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
//        int listPosition = info.position;
//        String valueselect = lstView.getAdapter().getItem(listPosition).toString();

        String uniname, scholurl;

        uniname = ((AdapterView.AdapterContextMenuInfo) item.getMenuInfo()).targetView.getTag(R.string.listtagname).toString();
        scholurl = ((AdapterView.AdapterContextMenuInfo) item.getMenuInfo()).targetView.getTag(R.string.listtagurl).toString();

        if (item.getItemId() == R.id.option1) {


            Toast.makeText(this, "Added  to Favourites", Toast.LENGTH_SHORT).show();

            String key = myRef.child("favourites").push().getKey();

            Map<String, String> val = new HashMap<String, String>();
            val.put("url", scholurl);
            val.put("name", uniname);
            Map<String, Object> childUpdates = new HashMap<String, Object>();
            childUpdates.put("/" + auth.getCurrentUser().getUid() + "/" + key, val);
            myRef.child("favourites").updateChildren(childUpdates);

        } else {


            if (!scholurl.startsWith("http")) {
                scholurl = "http://" + scholurl;
            }
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(scholurl));
            startActivity(i);
        }

        return super.onContextItemSelected(item);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.main, menu);


//        String[] strStates = new String[]{"AK",
//                "AZ",
//                "AR",
//                "CA",
//                "CO",
//                "CT",
//                "DE",
//                "DC",
//                "FL",
//                "GA",
//                "HI",
//                "ID",
//                "IL",
//                "IN",
//                "IA",
//                "KS",
//                "KY",
//                "LA",
//                "ME",
//                "MD",
//                "MA",
//                "MI",
//                "MN",
//                "MS",
//                "MO",
//                "MT",
//                "NE",
//                "NV",
//                "NH",
//                "NJ",
//                "NM",
//                "NY",
//                "NC",
//                "ND",
//                "OH",
//                "OK",
//                "OR",
//                "PA",
//                "RI",
//                "SC",
//                "SD",
//                "TN",
//                "TX",
//                "UT",
//                "VT",
//                "VA",
//                "WA",
//                "WV",
//                "WI",
//                "WY",
//                "GU",
//                "PR",
//                "VI"};
//
//
//
//        for (String state: strStates
//             ) {
//
//            menu.add(state);
//        }

        for (int i = 0; i < jsonobj.names().length(); i++) {

            try {

                menu.add((jsonobj.names().getString(i)).toString());

            } catch (JSONException ex) {
                Toast.makeText(this, "errror", Toast.LENGTH_SHORT).show();
            }
        }

        menu.add("reset");

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();


        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }

        if ((String) item.getTitle() == "reset") {

            resetStateScholarships();

        } else {
            try {
                filterStateScholarships((String) jsonobj.get((String) item.getTitle()));
            } catch (JSONException e) {
                e.printStackTrace();
            }
//            filterStateScholarships((String) item.getTitle());
        }

        return super.onOptionsItemSelected(item);
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_favourite) {

            Intent intent = new Intent(MainActivity.this, FavouriteActivity.class);
            startActivity(intent);

        } else if (id == R.id.nav_applied) {
            Intent intent = new Intent(MainActivity.this, AppliedActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_share) {

            shareApp();

        } else if (id == R.id.nav_logout) {

            auth.signOut();
            Intent intent = new Intent(MainActivity.this, LoginActivity.class);
            startActivity(intent);

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void resetStateScholarships() {

        MOBILE_OS.clear();
        names.clear();

        myRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {


                url = (String) dataSnapshot.child("url").getValue();
                if (url != null) {
                    url = url.replaceAll("\\s+", "");
                    if (url.trim() != "") {

                        MOBILE_OS.add(url);

                    }
                }
                name = (String) dataSnapshot.child("name").getValue();


                names.add(name);


            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        myRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                lstView.setAdapter(new MobileArrayAdapter(MainActivity.this, MOBILE_OS, names));

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }

    private void filterStateScholarships(String state) {

        MOBILE_OS.clear();
        names.clear();

        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setMessage("Loading com.scholardude.scholarships");
        progressDialog.show();


        myRef.orderByChild("state").equalTo(state).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                Iterable<DataSnapshot> iterdsnp = dataSnapshot.getChildren();
                for (DataSnapshot dtat0 : iterdsnp
                        ) {

//                    MOBILE_OS.add((String) dtat0.child("url").getValue());

                    url = (String) dtat0.child("url").getValue();
                    if (url != null) {
                        url = url.replaceAll("\\s+", "");
                        if (url.trim() != "") {

                            MOBILE_OS.add(url);

                        }
                    }

                    name = (String) dtat0.child("name").getValue();
                    names.add(name);

                }

                lstView.setAdapter(new MobileArrayAdapter(MainActivity.this, MOBILE_OS, names));
                progressDialog.hide();

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }


    protected void shareApp() {
        Intent share = new Intent(android.content.Intent.ACTION_SEND);
        share.setType("text/plain");
        share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
        share.putExtra(Intent.EXTRA_SUBJECT, "ScholarDude");
        String sharetxt = "Let me recommend you this application " + "http://scholardude.com/";
        share.putExtra(Intent.EXTRA_TEXT, sharetxt);
        startActivity(Intent.createChooser(share, "Share link!"));
    }
}
